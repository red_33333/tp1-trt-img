//=====GLOBAL=====\\

final int TICK_RATE = 240;

final float FRAME_RATE = 60;
final float FRAME_DELTA = 1000/FRAME_RATE;

final PVector GRAVITY = new PVector(0, 0.03);
final float AIR_DENSITY = 0.97;

final float PIXEL_TO_METER = 130;

final float TIME_RATIO = 1;

final float WIND_STRENGHT = 350;

//=====SCENE======\\
final float GRASS_HEIGHT_RATIO = 4;



//=====CANON=====\\

//VISUALS\\
final float CANON_POS_X_RATIO = 32;
final float CANON_POS_Y_RATIO = 32;

final float CANON_SIZE_X_RATIO = 8;
final float CANON_SIZE_Y_RATIO = 14;

//CONTROLS\\
final float CANON_MIN_ANGLE = -75;
final float CANON_MAX_ANGLE = -5;

final float CANON_ANGLE_STEP = 3;


//=====CANONBALL=====\\

//VISUALS\\
final float BALL_RADIUS_RATIO = 64; 


//PHYSICS\\
final float IRON_DENSITY = 7870; 
final float SHOOTING_VELOCITY = 800;

final float BALL_BOUNCE_FEEDBACK_RATIO = 0.9;



//=====FLUID=====\\

final float FLUID_SIZE_X_RATIO = 4;
final float FLUID_SIZE_Y_RATIO = 5;

final float FLUID_DENSITY_HIGH = 300;
final float FLUID_DENSITY_LOW = 150;

final float WATER_DENSITY = 997;



//=====TARGET=====\\

final float TARGET_AMOUNT_LOW = 15;
final float TARGET_AMOUNT_HIGH = 30;

//PHYSICS\\
final float GOLD_DENSITY = 19300;


//=====CLOUDS====\\

final float CLOUD_SPAWN_RATE = 50;


//=====VIGNETTE=====\\


final float VIGNETTE_DISTANCE_RATIO = 10;
