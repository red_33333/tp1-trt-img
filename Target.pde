class Target extends GObject{
  
  boolean active;
  boolean inFluid;
  
  boolean firstHitFluid;
  boolean confirmedHitFluid;
  
  Target(){
    
    float radius = random(height/45, height/30);
    
    size = new PVector(radius, radius);
    pos = new PVector(random(width - width/FLUID_SIZE_X_RATIO + radius, width - radius), random(radius, height - height/FLUID_SIZE_Y_RATIO - radius));
    
    density = GOLD_DENSITY/PIXEL_TO_METER;
    mass = (PI*sq(size.x)) * density;
    
    acc = new PVector();
    vel = new PVector();
    
    firstHitFluid = false;
    confirmedHitFluid = false;
    
    active = false;
    
  }
  
  boolean isActive(){return active;}
  
  PVector getPos(){return pos;}
  PVector getSize(){return size;}
  PVector getVel(){return vel;}
  
  boolean hitFluidEdge(){
    if(firstHitFluid){
      confirmedHitFluid = true;
      firstHitFluid = false;
      return true;
    }
    
    return false;
    
  }
  
  void applyAcc(PVector nacc){
    acc.add(nacc);
  }
  void applyForce(PVector f){
    PVector finalForce = f.copy();
    finalForce.div(mass);
    acc.add(finalForce);
  }
  void applyFriction(float f){
    acc.mult(f);
  }
  
  void applyFluid(float d){
    
    //f = -1 * density * v.magnetude * surface * v.normalize 
    float forceX = -1 * d * vel.mag() * (size.y) * (vel.normalize()).x;
    float forceY = -1 * d * vel.mag() * (size.x) * (vel.normalize()).y;
    
    applyForce(new PVector(forceX, forceY));
    
  }
  
  boolean collide(PVector opos, PVector osize){
    
    if(pos.x + size.x >= opos.x && pos.x + size.x <= opos.x + osize.x || pos.x - size.x >= opos.x && pos.x - size.x <= opos.x + osize.x){
      if((pos.y + size.y >= opos.y && pos.y + size.y <= opos.y + osize.y)){
        inFluid = true;
        return true;
      }
    }
    
    inFluid = false;
    return false;
    
  }
  
  boolean collideBall(PVector opos, float orad){
    
    if(dist(pos.x, pos.y, opos.x, opos.y) <= size.x + orad){
      active = true;
      return true;
    }
    
    return false;
    
  }
  
  
  void update(float delta){}
  
  void update(float delta, Fluid fluid){
    
    if(active){
      
      
      applyAcc(GRAVITY);
      if(!inFluid){
        applyForce(WIND);
      }
      applyFriction(AIR_DENSITY);
  
      if(fluid.isActive() && collide(fluid.getPos(), fluid.getSize())){
        if(!confirmedHitFluid){
          firstHitFluid = true;
        }
        applyFluid(fluid.getDensity());
        fluid.collide(this);    //=====================\\
      }else {
        firstHitFluid = false;
        confirmedHitFluid = false;
      }
      
      vel.add(acc);
      pos.add(vel);
      pos.add(PVector.mult(vel, delta/FRAME_DELTA));
      
      acc.mult(0);
      
      bounceWalls();
      
    }
    
  }
  
  void bounceWalls(){
    
    if(pos.x + size.x >= width){
      pos.x = width - size.x;
      vel.x *= -1 * bounceRatio;
    }else if(pos.x - size.x <= 0){
      pos.x = size.x + 1;
      vel.x *= -1 * bounceRatio;
    }
    
    if(pos.y + size.y >= height){
      pos.y = height - size.y;
      vel.y *= -1 * bounceRatio;
      if(inFluid){
        vel.x = 0;
      }
    }
    
  }
  
  
  void show(){
    
    noStroke();
    
    fill(0, 0, 255);
    ellipse(pos.x, pos.y, size.x*2, size.y*2);
    
    fill(218,165,32);
    ellipse(pos.x, pos.y, size.x*1.5, size.y*1.5);
    
    
    fill(255, 0, 0);
    ellipse(pos.x, pos.y, size.x*0.5, size.y*0.5);
    
  }
  
}
