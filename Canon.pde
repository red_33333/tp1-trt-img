class Canon extends GObject{
  
  float angle;
  
  
  ArrayList<Ball> balls;
  
  Canon(){
    
    size = new PVector(width/CANON_SIZE_X_RATIO, height/CANON_SIZE_Y_RATIO);
    
    pos = new PVector(width/CANON_POS_X_RATIO, height - height/CANON_POS_Y_RATIO - size.y);
    
    angle = -25;
    
    balls = new ArrayList<Ball>();
    
    
  }
  
  
  PVector getCanonTip(){
    return new PVector((pos.x + size.x/4) + cos(radians(angle)) * size.x, (pos.y) + sin(radians(angle)) * size.x);
  }
  float getAngle(){return angle;}
  
  void update(float delta){
    
    for(int i = balls.size() - 1; i >= 0; i--){
      
      Ball part = balls.get(i);
      
      
      part.update(delta);
      
      if(!part.is_alive()){
        balls.remove(i);
      }
      
    }
    
  }
  
  void update(float delta, Fluid fluid, ArrayList<Target> targets, ArrayList<Poof> poofs, ArrayList<Splash> splashes){
    
    for(int i = balls.size() - 1; i >= 0; i--){
      
      Ball part = balls.get(i);
      
      part.update(delta, fluid);
      if(part.hitFluidEdge()){
        int amount = floor(random(10, 20));
        
        for(int j = 0; j < amount ; j++){
          
          splashes.add(new Splash(part.getPos().x, part.getPos().y, part.getVel().x));
          
        }
      }
      
      if(part.spawnCloud()){
        poofs.add(new Poof(part.getPos().x, part.getPos().y, false, true));
        part.spawnedCloud();
      }
      
      for(Target opart : targets){
        
        if(!opart.isActive() && opart.collideBall(part.getPos(), part.getSize().x)){
          part.kill();
        }
        
      }
      
      if(!part.is_alive()){
        poofs.add(new Poof(part.getPos().x, part.getPos().y, part.hitTarget()));
        balls.remove(i);
      }
      
    }
    
  }
  
  
  void show(){
    
    for(Ball part : balls){
      
      part.show();
      
    }
    
    
    //canon
    noStroke();
    
    pushMatrix();
    
    translate(pos.x + size.x/4, pos.y);
    rotate(radians(angle));
    
    
    fill(75);
    
    rect(-size.x/4, -size.y/4, size.x + size.x/4, size.y/2);
    
    textAlign(LEFT, CENTER);
    fill(0);
    text("Vive la France", -size.x/4 + size.x/3, 0);
    
    stroke(25);
    strokeWeight(5);
    point(0, 0);
    
    popMatrix();
    
    
    //body
    noStroke();
    fill(54, 38, 27);
    rect(pos.x, pos.y, size.x, size.y);
    
    //wheel
    stroke(75);
    strokeWeight(size.x/35);
    ellipse(pos.x + size.x/6, pos.y + size.y, size.x/4, size.x/4);
    ellipse(pos.x + size.x - size.x/6, pos.y + size.y, size.x/4, size.x/4);
    
    //shaft
    fill(75);
    noStroke();
    ellipse(pos.x + size.x/6, pos.y + size.y, size.x/12, size.x/12);
    ellipse(pos.x + size.x - size.x/6, pos.y + size.y, size.x/12, size.x/12);
    
    //stroke(255, 0, 0);
    //strokeWeight(1);
    
    //line(pos.x + size.x/4, pos.y, pos.x + size.x/4 + (cos(radians(angle)) * 200), pos.y + (sin(radians(angle)) * 200));
    
    
    
  }
  
  
  
  
  
  void upAngle(){
    angle -= CANON_ANGLE_STEP;
    if(angle < CANON_MIN_ANGLE){
      angle = CANON_MIN_ANGLE;
    }
  }
  void downAngle(){
    angle += CANON_ANGLE_STEP;
    if(angle > CANON_MAX_ANGLE){
      angle = CANON_MAX_ANGLE;
    }
  }
  
  
  void shoot(){
    
    balls.add(new Ball(new PVector(pos.x + size.x/4, pos.y), size.x - size.x/4, angle));
    
  }
  
  
  
}
