class Vignette{
  
  PImage vignette;
  
  float ratio;
  float ratioGoal;
  
  float ratioSpeed;
  
  
  float flashStrength;
  
  Vignette(){
    
    vignette = createImage(width/2, height/2, ARGB);
    
    ratioGoal = 0.8;
    ratio = ratioGoal;
    
    ratioSpeed = 0.05;
    
    flashStrength = 0;
    
    drawVignette();
    
  }
  
  
  
  void flash(){
    
    flashStrength = 255;
    
  }
  
  
  void drawVignette(){
    
    
    for(int i = 0; i < vignette.width; i++){
      for(int j = 0; j < vignette.height; j++){
        
        vignette.set(i, j, color(flashStrength, dist(i, j, vignette.width/2, vignette.height/2) * 255 / (vignette.width/ratio)));
        
      }
    }
    
    
  }
  
  
  
  void show(){
    
    
    if(ratio != ratioGoal){
      
      if(ratio < ratioGoal){
        ratio += ratioSpeed;
      }else {
        ratio -= ratioSpeed;
      }
      
      if(abs(ratio - ratioGoal) < ratioSpeed){
        ratio = ratioGoal;
      }
      
      drawVignette();
      
    }else if(flashStrength > 0){
      drawVignette();
      flashStrength -= 6;
    }
    
    pushMatrix();
    translate(0, 0, 15);
    
    beginShape();
    texture(vignette);
    
    vertex(0, 0, 0, 0);
    vertex(width, 0, vignette.width, 0);
    vertex(width, height, vignette.width, vignette.height);
    vertex(0, height, 0, vignette.height);
    
    endShape();
    
    
    popMatrix();
    
  }
  
  
  void setRatio(int dist){
    
    if(dist <= VIGNETTE_DISTANCE_RATIO){
      
      float nratio = 3 - float(dist) * 3f / VIGNETTE_DISTANCE_RATIO;
      if(nratio < 1.2){
        nratio = 1.2;
      }
      
      if(ratio != nratio){
        ratioGoal = nratio;
        drawVignette();
      }
      
    }
      
  }
  
  
}
