class Enveloppe{
  
  
  Canon canon;
  Fluid fluid;
  
  ArrayList<Target> targets;
  
  
  ArrayList<Poof> poofs;
  ArrayList<Splash> splashes;
  
  
  Vignette vignette;
  
  boolean toggleFire;
  
  Enveloppe(){
    
    canon = new Canon();
    fluid = new Fluid(new PVector(width - width/FLUID_SIZE_X_RATIO, height - height/FLUID_SIZE_Y_RATIO), new PVector(width/FLUID_SIZE_X_RATIO, height/FLUID_SIZE_Y_RATIO));
    
    targets = new ArrayList<Target>();
    poofs = new ArrayList<Poof>();
    splashes = new ArrayList<Splash>();
    
    int targetAmount = floor(random(TARGET_AMOUNT_LOW, TARGET_AMOUNT_HIGH));
    
    for(int i = 0; i < targetAmount; i++){
      targets.add(new Target());
    }
    
    vignette = new Vignette();
    
  }
  
  
  
  void update(float delta){
    
    for (int i = splashes.size() - 1; i >= 0; i--) {

      Splash part = splashes.get(i);

      part.update(fluid);

      if (!part.is_alive()) {

        splashes.remove(i);
        
      }
      
    }
    
    for (int i = poofs.size() - 1; i >= 0; i--) {

      Poof part = poofs.get(i);

      part.update();

      if (!part.is_alive()) {

        poofs.remove(i);
        
      }
      
    }
    
    canon.update(delta, fluid, targets, poofs, splashes);
    
    for(Target part : targets){
      part.update(delta, fluid);
      if(part.hitFluidEdge()){
        int amount = floor(random(10, 20));
        
        for(int j = 0; j < amount ; j++){
          
          splashes.add(new Splash(part.getPos().x, part.getPos().y, part.getVel().x, true));
          
        }
      }
    }
    fluid.update();
    
    if(toggleFire){
      canonShoot();
    }
    
  }
  
  
  
  void show(){
    
    background(135, 206, 235);

    fill(135, 206, 100);
    noStroke();

    rect(0, height - height/GRASS_HEIGHT_RATIO, width, height/GRASS_HEIGHT_RATIO);
    
    for(Splash part: splashes){
      
      part.show();
      
    }
    
    for (Poof part : poofs) {
      
      part.show();
      
    }
    
    canon.show();
    
    for(Target part : targets){
      
      part.show();
      
    }
    
    fluid.show();
    
    
    
    fill(0);
    textAlign(LEFT, CENTER);
    text("WIND: " + WIND.x, 0, 15);
    
    fill(0);
    textAlign(LEFT, CENTER);
    text("FrameRate: " + nf(frameRate, 0, 0), 0, 30);
    
    vignette.show();
    
    
  }
  
  
  
  void interact(char k){
    
    if(k == 'a' || k == 'A'){
      canonUp();
    }else if(k == 'd' || k == 'D'){
      canonDown();
    }else if(k == 'f' || k == 'F'){
      toggleFluid();
    }else if(k == ' '){
      canonShoot();
    }else if(k == 'b' || k == 'B'){
      toggleFire = !toggleFire;
    }
    
  }
  
  
  void canonUp(){
    canon.upAngle();
  }
  void canonDown(){
    canon.downAngle();
  }
  void canonShoot(){
    canon.shoot();
    poofs.add(new Poof(canon.getCanonTip().x, canon.getCanonTip().y, canon.getAngle()));
  }
  void toggleFluid(){
    fluid.toggleActive();
  }
  
  
  void setWind(int dir){
    if(dir == RIGHT){
      WIND.x = -WIND_STRENGHT;
    }else if(dir == LEFT){
      WIND.x = WIND_STRENGHT;
    }
  }
  
  
  void killWind(){
    
    WIND.x = 0;
    
  }
    
  
  
}
