class Splash{
  
  PVector pos;
  PVector vel;
  PVector acc;
  
  float size;
  float lifeTime;
  float invTime;
  
  
  float mass;
  
  boolean alive;
  
  Splash(float x, float y, float velX){
    
    pos = new PVector(x, y);
    acc = new PVector(velX + random(GRAVITY.y * (-abs(velX)*15), GRAVITY.y * (abs(velX)*15)), random(-GRAVITY.y * 100, -GRAVITY.y * 50));
    vel = new PVector(0, 0);
    
    lifeTime = currentTime;
    invTime = 350;;
    
    size = floor(random(height/50, height/75));
    
    
    mass = (PI*sq(size)) * (WATER_DENSITY/PIXEL_TO_METER);
    
    
    alive = true;
    
  }
  
  Splash(float x, float y, float velX, boolean vertical){
    
    pos = new PVector(x, y);
    if(!vertical){
      acc = new PVector(velX + random(GRAVITY.y * (-abs(velX)*15), GRAVITY.y * (abs(velX)*15)), random(-GRAVITY.y * 100, -GRAVITY.y * 50));
    }else {
      acc = new PVector(random(GRAVITY.y * -10, GRAVITY.y * 10), random(-GRAVITY.y * 100, -GRAVITY.y * 50));
    }
    vel = new PVector(0, 0);
    
    lifeTime = currentTime;
    invTime = 350;
    
    
    size = floor(random(height/50, height/75));
    
    mass = (PI*sq(size)) * (WATER_DENSITY/PIXEL_TO_METER);
    
    
    alive = true;
    
  }
  
  
  boolean is_alive(){return alive;}
  PVector getPos(){return pos;}
  float getSize(){return size;}
  
  void applyForce(PVector f){
    PVector finalForce = f.copy();
    finalForce.div(mass);
    acc.add(finalForce);
  }
  void addAcc(PVector oacc){
    acc.x += oacc.x;
    acc.y += oacc.y;
  }
  
  void update(Fluid fluid){
    
    addAcc(GRAVITY);
    applyForce(WIND);
    
    vel.add(acc);
    pos.add(vel);
    
    acc.mult(0);
    
    if(pos.y - size/2 > height){
      alive = false;
    }
    if(pos.x + size/2 < 0){
      pos.x = size/2;
    }
    if(pos.x - size/2 > width){
      pos.x = width - size/2;
    }
    
    if(currentTime - lifeTime >= invTime && collide(fluid.getPos(), fluid.getSize())){
      alive = false;
    }
    
  }
  
  boolean collide(PVector opos, PVector osize){
    
    if((pos.x + size >= opos.x && pos.x + size <= opos.x + osize.x) || (pos.x - size >= opos.x && pos.x - size <= opos.x + osize.x)){
      if((pos.y >= opos.y && pos.y <= opos.y + osize.y)){
        return true;
      }
    }
    
    return false;
    
  }
  
  void show(){
    
    noStroke();
    fill(150, 100, 0, 100);
    ellipse(pos.x, pos.y, size, size);
    
  }
  
  
  
}
