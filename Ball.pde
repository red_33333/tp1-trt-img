class Ball extends GObject{
  
  boolean alive;
  boolean inFluid;
  
  boolean hitTarget;
  
  boolean firstHitFluid;
  boolean confirmedHitFluid;
  
  float lastAddedCloud;
  
  Ball(PVector origin, float canonLength, float angle){
    
    density = IRON_DENSITY/PIXEL_TO_METER;
    
    size = new PVector(height/BALL_RADIUS_RATIO, height/BALL_RADIUS_RATIO);
    
    mass = (PI*sq(size.x)) * density;
    if(mass <= 0){
      println("NULL MASS");
      exit();
    }
    
    pos = new PVector(origin.x + cos(radians(angle)) * canonLength, origin.y + sin(radians(angle)) * canonLength);
    acc = new PVector(cos(radians(angle)) * ((SHOOTING_VELOCITY/PIXEL_TO_METER) * width / 800), sin(radians(angle)) * ((SHOOTING_VELOCITY/PIXEL_TO_METER) * width / 800));
    
    vel = new PVector(0, 0);
    
    bounceRatio = BALL_BOUNCE_FEEDBACK_RATIO;
    
    alive = true;
    inFluid = false;
    
    hitTarget = false;
    
    firstHitFluid = false;
    confirmedHitFluid = false;
    
    lastAddedCloud = currentTime;
    
  }
  
  boolean is_alive(){return alive;}
  boolean hitTarget(){return hitTarget;}
  PVector getPos(){return pos;}
  PVector getSize(){return size;}
  PVector getVel(){return vel;}
  
  boolean hitFluidEdge(){
    if(firstHitFluid){
      confirmedHitFluid = true;
      firstHitFluid = false;
      return true;
    }
    
    return false;
    
  }
  
  boolean spawnCloud(){return (currentTime - lastAddedCloud >= CLOUD_SPAWN_RATE);}
  void spawnedCloud(){lastAddedCloud = currentTime;}
  
  void kill(){alive = false; hitTarget = true;}
  
  
  void applyAcc(PVector nacc){
    acc.add(nacc);
  }
  void applyForce(PVector f){
    PVector finalForce = f.copy();
    finalForce.div(mass);
    acc.add(finalForce);
  }
  void applyFriction(float f){
    acc.mult(f);
  }
  
  void applyFluid(float d){
    
    //f = -1 * density * v.magnetude * surface * v.normalize 
    float forceX = -1 * d * vel.mag() * (size.y) * (vel.normalize()).x;
    float forceY = -1 * d * vel.mag() * (size.x) * (vel.normalize()).y;
    
    applyForce(new PVector(forceX, forceY));
    
  }
  
  boolean collide(PVector opos, PVector osize){
    
    if(pos.x + size.x >= opos.x && pos.x + size.x <= opos.x + osize.x || pos.x - size.x >= opos.x && pos.x - size.x <= opos.x + osize.x){
      if((pos.y + size.y >= opos.y && pos.y + size.y <= opos.y + osize.y)){
        inFluid = true;
        return true;
      }
    }
    
    inFluid = false;
    return false;
    
  }
  
  void update(float delta){
    
    applyAcc(GRAVITY);
    applyForce(WIND);
    applyFriction(AIR_DENSITY);

    
    vel.add(acc);
    pos.add(vel);
    pos.add(PVector.mult(vel, delta/FRAME_DELTA));
    
    acc.mult(0);
    
    bounceWalls();
    
    
  }
  
  void update(float delta, Fluid fluid){
    
    applyAcc(GRAVITY);
    applyForce(WIND);
    applyFriction(AIR_DENSITY);

    if(fluid.isActive() && collide(fluid.getPos(), fluid.getSize())){
      if(!confirmedHitFluid){
        firstHitFluid = true;
      }
      applyFluid(fluid.getDensity());
      fluid.collide(this);
    }else {
      firstHitFluid = false;
      confirmedHitFluid = false;
    }
    
    vel.add(acc);
    pos.add(vel);
    pos.add(PVector.mult(vel, delta/FRAME_DELTA));
    
    acc.mult(0);
    
    bounceWalls();
    
  }
  
  
  void bounceWalls(){
    
    if(pos.x + size.x >= width){
      pos.x = width - size.x;
      vel.x *= -1 * bounceRatio;
    }else if(pos.x - size.x <= 0){
      pos.x = size.x + 1;
      vel.x *= -1 * bounceRatio;
    }
    
    if(pos.y + size.y >= height){
      pos.y = height - size.y;
      vel.y *= -1 * bounceRatio;
      if(inFluid){
        alive = false;
      }
    }else if(pos.y - size.y <= 0){
      pos.y = size.y + 1;
      vel.y *= -1 * bounceRatio;
    }
    
  }
  
  
  void show(){
    
    fill(200, 50, 0);
    ellipse(pos.x, pos.y, size.x*2, size.y*2);
    
  }
  
  
}
