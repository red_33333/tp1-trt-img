class Fluid{
  
  float density;
  PVector pos;
  PVector size;
  
  float[] waveOffset;
  float[] waveVel;

  boolean active;
  
  Fluid(){
    
    density = random(FLUID_DENSITY_LOW, FLUID_DENSITY_HIGH)/PIXEL_TO_METER;
    size = new PVector(width, height/4);
    pos = new PVector(0, height - size.y);
    
    waveOffset = new float[int(size.x)/3];
    waveVel = new float[int(size.x)/3];
    
    for(int i = 0; i < waveOffset.length; i++){
      waveOffset[i] = 0;
      waveVel[i] = 0;
    }

    active = true;
    
    
  }
  
  Fluid(PVector npos, PVector nsize){
    
    density = random(FLUID_DENSITY_LOW, FLUID_DENSITY_HIGH)/PIXEL_TO_METER;
    size = new PVector(nsize.x, nsize.y);
    pos = new PVector(npos.x, npos.y);
    
    
    waveOffset = new float[int(size.x)/3];
    waveVel = new float[int(size.x)/3];
    
    for(int i = 0; i < waveOffset.length; i++){
      waveOffset[i] = 0;
      waveVel[i] = 0;
    }
    
    active = true;
    
  }
  
  boolean isActive(){return active;}
  PVector getPos(){return pos;}
  PVector getSize(){return size;}
  float getDensity(){return density;}
  
  
  void toggleActive(){active = !active;}
  
  
  void collide(Ball ball){
    
    if(pos.y >= ball.getPos().y - ball.getSize().y && pos.y <= ball.getPos().y + ball.getSize().y){
      
      int pos = floor((ball.getPos().x-(width - width/FLUID_SIZE_X_RATIO))/3);
      
      for(int i = 0; i < waveOffset.length; i++){
        
        if(dist(pos, 0, i, 0) < ball.getSize().x){
          
          float velOffset = (ball.getSize().x - dist(pos, 0, i, 0))/2;
          if(pos >= 0 && pos < waveOffset.length){
            waveVel[i] = velOffset;
          }
          
        }
        
        
      }
      
      
      
    }
    
  }
  
  void collide(Target target){
    
    if(pos.y >= target.getPos().y - target.getSize().y && pos.y <= target.getPos().y + target.getSize().y){
      
      int pos = floor((target.getPos().x-(width - width/FLUID_SIZE_X_RATIO))/3);
      
      for(int i = 0; i < waveOffset.length; i++){
        
        if(dist(pos, 0, i, 0) < target.getSize().x){
          
          float velOffset = (target.getSize().x - dist(pos, 0, i, 0))/2;
          if(pos >= 0 && pos < waveOffset.length){
            waveVel[i] = velOffset;
          }
          
        }
        
        
      }
      
      
      
    }
    
  }
  
  
  
  
  void update(){
    
    if(active){
        
      for(int i = 0; i < waveOffset.length; i++){
        
        for(int j = -1; j <= 1; j++){
          if(i + j >= 0 && i + j < waveOffset.length){
            if(abs(waveVel[i+j]) > abs(waveVel[i])){
              waveVel[i] = waveVel[i+j]*0.75;
            }
          }
        }
        
        if(waveOffset[i] > 0){
          waveVel[i] -= GRAVITY.y*6;
        }else if(waveOffset[i] < 0){
          waveVel[i] += GRAVITY.y;
        }
        
        waveVel[i] *= 0.97;
        waveOffset[i] += waveVel[i];
        
      }

    }
    
  }
  
  void show(){
    
    if(active){

      fill(150, 100, 0, 100);
      noStroke();
      
      beginShape();
      
      for(int i = 0; i < waveOffset.length; i++){
        vertex(pos.x + i * 3, pos.y + waveOffset[i]);
      }
      
      vertex(pos.x + size.x, pos.y);
      vertex(pos.x + size.x, pos.y + size.y);
      vertex(pos.x, pos.y + size.y);
      
      
      endShape(CLOSE);
      
      textAlign(CENTER, CENTER);
      fill(0);
      text("Fluid density: " + nf(density, 0, 2) +"\n Dominick Viau-Bissonnette", pos.x + size.x/2, pos.y + size.y/2);
      
    }

  }
  
  
  
}
