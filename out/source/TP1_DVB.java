import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class TP1_DVB extends PApplet {


public void setup(){
  
  surface.setResizable(true);
  
  
  surface.setSize(800, 450);
  
  
  env = new Enveloppe();
  
  frameRate(TICK_RATE);
  
}



public void draw(){
  
  timeCalc();
  
  
  env.update(deltaTime);
  
  
  if(currentTime - lastRefresh >= FRAME_DELTA){
    
    env.show();
    
    lastRefresh = currentTime;
    
  }
  
  
}


public void keyPressed(){
  
  
  
}
final int TICK_RATE = 240;

final float FRAME_RATE = 60;
final float FRAME_DELTA = 1000/FRAME_RATE;

final PVector GRAVITY = new PVector(0, 1);
final float AIR_DENSITY = 0.97f;


final float PIXEL_TO_METER = 100;



//SCENE
final float GRASS_HEIGHT_RATIO = 4;
class Enveloppe{
  
  
  Enveloppe(){
    
    
    
  }
  
  
  
  public void update(float delta){
    
    
    
    
  }
  
  
  
  public void show(){
    
    background(135, 206, 235);

    fill(135, 206, 100);
    noStroke();

    rect(0, height - height/GRASS_HEIGHT_RATIO, width, height/GRASS_HEIGHT_RATIO);
    
    
  }
  
  
  
}
abstract class Object{
  
  PVector pos;
  PVector size;
  
  PVector vel;
  PVector acc;
  
  
  float mass;
  float density;
  
  float bounceRatio;
  
  int fillColor;
  int strokeColor;
  boolean doFill;
  boolean doStroke;
  
  
  public abstract void update(float delta);
  
  
  public abstract void show();
  
  
  
}
float currentTime = 0;
float pastTime = 0;

float deltaTime = 0;


float lastRefresh = 0;

public void timeCalc(){
  
  
  pastTime = currentTime;
  currentTime = millis();
  
  deltaTime = currentTime - pastTime;
  
}
Enveloppe env;


PVector WIND = new PVector(0, 0);
  public void settings() {  size(10, 10); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "TP1_DVB" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
