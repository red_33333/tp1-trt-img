class Poof{
  
  ArrayList<PoofCloud> clouds;
  PVector pos;
  
  boolean alive;
  
  Poof(float x, float y, boolean ngolden){
    
    pos = new PVector(x, y);
    clouds = new ArrayList<PoofCloud>();
    int amount = int(random(10, 15));
    for(int i = 0; i < amount; i++){
      
      clouds.add(new PoofCloud(pos.x, pos.y, ngolden));
      
    }
    
    alive = true;
    
  }
  
  Poof(float x, float y, boolean ngolden, boolean small){
    
    pos = new PVector(x, y);
    clouds = new ArrayList<PoofCloud>();
    int amount = int(random(10, 15));
    if(small){
      amount = int(random(3, 5));
    }
    for(int i = 0; i < amount; i++){
      
      clouds.add(new PoofCloud(pos.x, pos.y, ngolden));
      
    }
    
    alive = true;
    
  }
  
  Poof(float x, float y, float angle){
    
    pos = new PVector(x, y);
    clouds = new ArrayList<PoofCloud>();
    int amount = int(random(25, 50));
    for(int i = 0; i < amount; i++){
      
      clouds.add(new PoofCloud(pos.x, pos.y, angle));
      
    }
    
    alive = true;
    
  }
  
  boolean is_alive(){return alive;}
  
  
  void update(){
    
    for(int i = clouds.size() - 1; i >= 0; i--){
      
      PoofCloud part = clouds.get(i);
      
      part.update();
      
      if(!part.is_alive()){
        clouds.remove(i);
      }
      
    }
    
    if(clouds.size() <= 0){
      alive = false;
    }
    
  }
  
  
  void show(){
    
    for(PoofCloud part : clouds){
      
      part.show();
      
    }
    
  }
  
}
