abstract class GObject{
  
  PVector pos;
  PVector size;
  
  PVector vel;
  PVector acc;
  
  
  float mass;
  float density;
  
  float bounceRatio;
  
  color fillColor;
  color strokeColor;
  boolean doFill;
  boolean doStroke;
  
  
  abstract void update(float delta);
  
  
  abstract void show();
  
  
  
}
