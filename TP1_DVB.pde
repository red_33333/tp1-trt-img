
void setup(){
  
  //surface.setResizable(true);
  //size(800, 450);
  size(800, 450, P3D);
  
  //fullScreen(P3D);
  
  //surface.setSize(800, 450);
  
  hint(DISABLE_OPTIMIZED_STROKE);
  
  env = new Enveloppe();
  
  frameRate(TICK_RATE);
  
}






void draw(){
  
  timeCalc();
  
  env.update(deltaTime);
  
  if(currentTime - lastRefresh >= FRAME_DELTA){
    
    env.show();
    
    lastRefresh = currentTime;
    
  }
  
}


void keyPressed(){
  
  if(key == CODED){
    
  }else {
    if(key == 'r'){
      env = new Enveloppe();
    }else {
      env.interact(key);
    }
  }
  
}


void mousePressed(){
  
  env.setWind(mouseButton);
  
}


void mouseReleased(){
  
  env.killWind();
  
}
