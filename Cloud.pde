class PoofCloud{
  
  float floorY;
  PVector pos;
  PVector vel;
  PVector acc;
  float size;
  
  float mass;
  
  int life;
  
  boolean golden;
  boolean alive;
  
  PoofCloud(float x, float y, boolean ngolden){
    
    floorY = y;
    pos = new PVector(x, y);
    vel = new PVector(random(-1.5 * width/800, 1.5 * width/800), random(1 * width/800, 4 * width/800));
    acc = new PVector(0, 0);
    if(!ngolden){
      vel.x /= 3;
    }
    size = random(9 * width/800, 24 * width/800);
    
    
    mass = (PI*sq(size)) * 1.6;
    
    life = 255;
    
    golden = ngolden;
    
    alive = true;
    
  }
  
  PoofCloud(float x, float y, float angle){
    
    floorY = y;
    pos = new PVector(x, y);
    
    float angleRange = 15;
    
    vel = new PVector(cos(radians(angle + random(-angleRange, angleRange))) * (2 * width/800), sin(radians(angle + random(-angleRange, angleRange))) * (2 * width/800));
    
    acc = new PVector(0, 0);
    size = random(4 * width/800, 14 * width/800);
    
    mass = (PI*sq(size)) * 1.6;
    
    life = 255;
    
    golden = false;
    
    alive = true;
    
  }
  
  boolean is_alive(){return alive;}
  
  void show(){
    
    if(golden){
      //fill(255,223,0, life);
      fill(218,165,32, life);
    }else {
      fill(150, life);
    }
    noStroke();
    
    
    float mult = 1;
    if(golden){
      mult = 2;
    }
    
    ellipse(pos.x, pos.y, size * mult, size * mult);
    
  }
  
  void applyForce(PVector f){
    PVector finalForce = f.copy();
    finalForce.div(mass);
    acc.add(finalForce);
  }
  
  void update(){
    
    
    applyForce(WIND);
    vel.add(acc);
    acc.mult(0);
    
    if(!golden){
      pos.add(vel);
    }else {
      pos.x += vel.x/2;
      pos.y += (vel.y - 4)/2;
    }
    
    if(pos.y > floorY && !golden){
      vel.y = 0;
    }
    
    if(vel.y > -3){
      vel.y -= 0.01;
    }
    
    if(!golden){
      life -= 5;
    }else {
      life -= 1.5;
    }
    
    if(life <= 0){
      alive = false;
    }
    
  }
  
}
